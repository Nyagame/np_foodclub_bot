# np_foodclub_bot

A simple script that takes bets from ~boochi_target and places them. It uses selenium and the Chrome Driver (alternatively, you can use firefox or phantomjs instead) to interact with both ~boochi_target's page and neopets itself.

## Using the script

1. In main.py, pass to the constructor of the Neodriver class the path to Chrome Driver.

e.g: 
```python
player = np.Neodriver('../selenium-driver/chromedriver')
```

2. Invoke main.py passing as arguments your user and password.

e.g:

```
python main.py user pass
```

3. Go get a cup of coffe and when you're back, it'll be done. =)

## To do

- [x] Food Club auto-bet
- [ ] Auto-collect winnings
- [ ] Exception Handling
    - [ ] Not enough Neopoints to make 10 bets
    - [ ] Wrong round bets
    - [ ] Food Club closed
    - [ ] etc
- [ ] Good documentation
    - [x] At least have some documentation