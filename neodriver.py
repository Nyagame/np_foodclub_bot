from selenium import webdriver
from selenium.webdriver.common import action_chains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import NoSuchElementException
import time
import re

class Neodriver:
    """Neodriver is a class that interacts with the browser via Selenium to 
    grab bets for Food Club and place them into Neopets."""

    def __init__(self, driver_path):
        self.driver = webdriver.Chrome(driver_path)
        self.final_bets = []

    def print_bets(self):
        """Prints all the bets already fetched with pull_bets() along with their index."""
        for i in range(len(self.final_bets)):
            print(i, '  =  ', self.final_bets[i])

    def parse_line(self, line):
        """Uses regex to separate the Arena from the Pirate into two variables inside a
        dict pair where the key is the Arena and the value is the Pirate."""
        match_line = re.search(r'(.*): (.*)', str(line), re.M|re.I)   
        return {match_line.group(1): match_line.group(2)}

    def parse_bet(self, lines):
        """Grabs all the separate (Arena, Pirate) pairs from a single bet and joins
        them together into one dict that contains the whole bet."""
        new_bet = dict()
        for i in lines:
            new_bet.update(self.parse_line(i))
        return new_bet

    def split_bets(self, bets):
        """Splits the bet string fetched into (Arena, Pirate) pairs and feeds them
        to parse_bet(). Returns a list with all the bets."""
        final_bet = []
        for i in bets:
            lines = i.split("\n")
            final_bet.append(self.parse_bet(lines))
        return final_bet

    def pull_bets(self, bet_page):
        """Gets the bets from the bet_page provided. It was made to be used with
        ~Boochi_target's page, but it can be adapted to work with other betters
        with some modifications."""
        self.driver.get(bet_page)
        bets = []

        for i in range(0, 10):
            bet_xpath = '/html/body/center[3]/center[1]/center[1]/table/tbody/tr[' + str(i + 3) + ']/td[2]'
            bet_line = self.driver.find_element_by_xpath(bet_xpath)
            bets.append(bet_line.text)

        self.final_bets = self.split_bets(bets)
        return self.final_bets

    def login(self, username, password):
        """Receiving username and password, logs into Neopets."""
        self.driver.get('http://www.neopets.com/login/index.phtml')
        username_input = self.driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr/td/div[3]/div[4]/form/div/div[1]/div[2]/input')
        password_input = self.driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr/td/div[3]/div[4]/form/div/div[2]/div[2]/input')
        action = action_chains.ActionChains(self.driver)
        action.move_to_element(username_input).click().send_keys(username)
        action.move_to_element(password_input).click().send_keys(password)
        action.send_keys(Keys.RETURN)
        action.perform()

    def _get_place(self, place):
        """Returns the Arena ID."""
        BETTING_PLACES = [
            'Shipwreck', # index = 0
            'Lagoon',
            'Treasure Island',
            'Hidden Cove',
            "Harpoon Harry's" # index = 4
        ]
        return BETTING_PLACES.index(place)

    def _get_pirate_id(self, pirate):
        """Returns the Pirate ID."""
        PIRATE_LIST = [
            "",
            "Scurvy Dan the Blade",
            "Young Sproggie",
            "Orvinn the First Mate",
            "Lucky McKyriggan",
            "Sir Edmund Ogletree",
            "Peg Leg Percival",
            "Bonnie Pip Culliford",
            "Puffo the Waister",
            "Stuff-A-Roo",
            "Squire Venable",
            "Captain Crossblades",
            "Ol' Stripey",
            "Ned the Skipper",
            "Fairfax the Deckhand",
            "Gooblah the Grarrl",
            "Franchisco Corvallio",
            "Federismo Corvallio",
            "Admiral Blackbeard",
            "Buck Cutlass",
            "The Tailhook Kid"
        ]
        return PIRATE_LIST.index(pirate)

    def _get_select_path(self, key):
        """Returns the paths to the Input Box and Select Box from the Bet Page."""
        xpath_index = 3 + self._get_place(key)
        select_path = [
            '//*[@id="content"]/table/tbody/tr/td[2]/center[4]/form/table[1]/tbody/tr[' + str(xpath_index) + ']/td[1]/input',
            '//*[@id="content"]/table/tbody/tr/td[2]/center[4]/form/table[1]/tbody/tr[' + str(xpath_index) + ']/td[2]/select'
        ]
        return select_path

    def _get_max_bet_amount(self):
        """Returns the text from the element with the maximum bet amount."""
        maximum_bet_amount = '//*[@id="content"]/table/tbody/tr/td[2]/p[4]/b'
        return self.driver.find_element_by_xpath(maximum_bet_amount).text

    def _get_neopoints(self):
        """Returns the text from the element with your neopoints."""
        neopoints = '//*[@id="npanchor"]'
        return self.driver.find_element_by_xpath(neopoints).text

    def _get_bet_amount_input(self):
        return self.driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr/td[2]/center[4]/form/table[2]/tbody/tr[3]/td[1]/input')

    def _get_bet_send_button(self):
        return self.driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr/td[2]/center[4]/form/p[3]/input[2]')

    def h_bet_selection(self, key, value):
        """Handles the clicking into the Input Box and Selector in the
        Bet Page."""
        input_box, select_box  = self._get_select_path(key)
        self.driver.find_element_by_xpath(input_box).click()
        selector = Select(self.driver.find_element_by_xpath(select_box))
        selector.select_by_value(str(self._get_pirate_id(value)))

    def h_bet_perform(self):
        """Handles the action of inserting the maximum bet into the bet input box."""
        action = action_chains.ActionChains(self.driver)
        action.move_to_element(self._get_bet_amount_input()).click()
        action.send_keys(str(self._get_max_bet_amount()))
        action.perform()

    def h_bet_send(self):
        """Handles the action of sending the form containing the bet."""
        action = action_chains.ActionChains(self.driver)
        action.move_to_element(self._get_bet_send_button()).click()
        action.perform()


    def make_bets(self):
        """Iterates through the bets fetched and places them into the Neopets UI."""
        for index in range(len(self.final_bets)):
            self.driver.get('http://www.neopets.com/pirates/foodclub.phtml?type=bet')
            for key, value in self.final_bets[index].items():
                try:
                    self.h_bet_selection(key, value)
                except NoSuchElementException as e:
                    print ("Element was found:", e)

            self.h_bet_perform()
            self.h_bet_send()


    def quit(self):
        self.driver.quit()