import sys
import neodriver as np

if __name__ == "__main__":
    username = sys.argv[1]
    password = sys.argv[2]
    player = np.Neodriver('../selenium-driver/chromedriver')
    player.pull_bets('http://www.neopets.com/~boochi_target')
    player.login(username, password)
    player.make_bets()
    player.quit()